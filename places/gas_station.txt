{
   "html_attributions" : [],
   "results" : [
      {
         "geometry" : {
            "location" : {
               "lat" : 10.8384128,
               "lng" : 106.7668815
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 10.83975097989272,
                  "lng" : 106.7681888298927
               },
               "southwest" : {
                  "lat" : 10.83705132010728,
                  "lng" : 106.7654891701073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/gas_station-71.png",
         "id" : "73965a3918eb559ef39aa7b61050a6329eca3ea6",
         "name" : "Trạm xăng dầu Bình Thọ",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 2448,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/105883795095465921197/photos\"\u003eHậu Bùi\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAB9IsNoLxOcapwXSlu7kMSou5dmWeEUMBKzjxFwu6N4ro3g9pQfBlK4CjX25hqJqUbeNl4O_cmL4N60j1lbTYq9ieX-Kh63dtJCq7zHRodqy5JIhyViGRR8Ozrgg4C0kuEhBRHlTYOY_c2D2vLYIOFd32GhT19sV6P9gMQavPCQbsCVEr-OWzQQ",
               "width" : 3264
            }
         ],
         "place_id" : "ChIJ5wl8jagndTERXXNqPopwQio",
         "plus_code" : {
            "compound_code" : "RQQ8+9Q Bình Thọ, Thủ Đức, Hồ Chí Minh",
            "global_code" : "7P28RQQ8+9Q"
         },
         "rating" : 3.8,
         "reference" : "ChIJ5wl8jagndTERXXNqPopwQio",
         "scope" : "GOOGLE",
         "types" : [ "gas_station", "point_of_interest", "establishment" ],
         "user_ratings_total" : 117,
         "vicinity" : "2B Đặng Văn Bi"
      }
   ],
   "status" : "OK"
}