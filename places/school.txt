{
   "html_attributions" : [],
   "results" : [
      {
         "geometry" : {
            "location" : {
               "lat" : 10.7829634,
               "lng" : 106.6948371
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 10.78447297989272,
                  "lng" : 106.6959430798927
               },
               "southwest" : {
                  "lat" : 10.78177332010728,
                  "lng" : 106.6932434201073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/school-71.png",
         "id" : "96821684c831e0610bb6ceb9bbd3fe5fd922339c",
         "name" : "Trường Đại học Kinh tế TP. Hồ Chí Minh - Cơ sở A",
         "photos" : [
            {
               "height" : 480,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/101862164169278313945/photos\"\u003eTrung Hậu Nguyễn\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAc0Eg4Un45qAf0o4tA1gjM2pEmFCH7sEu9H780i2E_OQDTBMsN_HDsi7QBO4A2nnwsj_UVB5okjjcPtHO3wSmvYnng4E0brdmdw6TzYYoFMx3im-pQmZfcuriMW-UtMVFEhDXSayYb4t1EbVM4ZSthU7QGhSL58T0oq_glmmPUXClWTupapDcUg",
               "width" : 781
            }
         ],
         "place_id" : "ChIJmapMxzYvdTERpSDvLmpEPHQ",
         "plus_code" : {
            "compound_code" : "QMMV+5W Phường 6, Quận 3, Hồ Chí Minh",
            "global_code" : "7P28QMMV+5W"
         },
         "rating" : 4.5,
         "reference" : "ChIJmapMxzYvdTERpSDvLmpEPHQ",
         "scope" : "GOOGLE",
         "types" : [ "university", "point_of_interest", "school", "establishment" ],
         "user_ratings_total" : 576,
         "vicinity" : "59C Nguyễn Đình Chiểu"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 10.740807,
               "lng" : 106.615854
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 10.74211587989272,
                  "lng" : 106.6171895298927
               },
               "southwest" : {
                  "lat" : 10.73941622010728,
                  "lng" : 106.6144898701073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/school-71.png",
         "id" : "8e1f42fc9856b1a74dde560b29e148428f1cdb27",
         "name" : "Trường Cao đẳng Quốc Tế TP.HCM",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 2160,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/104130572177853396100/photos\"\u003eQuân Nguyễn Thanh\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAeymJTq02IEIzCMNSLIOtVO5yK6irD90Q9hoiCjvpNdwHFI4x8BtcdAaAsJA5IIEZ8KONJ4bd8s3k67Noi9mklSUau4Q4EPQufL8PKDSYlDjdQ5Ev7Tk-XaR7qzpANunBEhAWkSmZ2NCcVKVP346R3qX-GhQq_Un9JJ8BdTbc_C-R3948C32fQg",
               "width" : 3840
            }
         ],
         "place_id" : "ChIJ04yIv88tdTERPvqFfHM-R1c",
         "plus_code" : {
            "compound_code" : "PJR8+88 An Lạc, Bình Tân, Hồ Chí Minh",
            "global_code" : "7P28PJR8+88"
         },
         "rating" : 4.6,
         "reference" : "ChIJ04yIv88tdTERPvqFfHM-R1c",
         "scope" : "GOOGLE",
         "types" : [ "school", "point_of_interest", "establishment" ],
         "user_ratings_total" : 78,
         "vicinity" : "460D Kinh Dương Vương"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 10.79546,
               "lng" : 106.659742
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 10.79684642989272,
                  "lng" : 106.6610723798927
               },
               "southwest" : {
                  "lat" : 10.79414677010728,
                  "lng" : 106.6583727201073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/school-71.png",
         "id" : "df39fb86cad74f563741697caf325aef9a0fb864",
         "name" : "Trường Trung cấp Du lịch và Khách sạn Saigontourist",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 480,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/108592240053293321110/photos\"\u003eA Google User\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAACwKYkpBUv0KDgc6eh8p2D0IEe-S9oNdcoF9N_9IMqpNWSBcA31S7nRmQRfSkLWwuAkirUIiF143pFTExMSpVjDTGPXDJhXZTPgLRgkc7aLgR85MM_Nro8wMHGFhcwXOTEhA0rk6lnssYlO95nWQqrwjLGhQxd9OnkX3PoUssfeMY2_O2cU4k1g",
               "width" : 1200
            }
         ],
         "place_id" : "ChIJEWxTkzMpdTEReNz4_TMeW_Q",
         "plus_code" : {
            "compound_code" : "QMW5+5V Phường 4, Tân Bình, Hồ Chí Minh",
            "global_code" : "7P28QMW5+5V"
         },
         "rating" : 4.6,
         "reference" : "ChIJEWxTkzMpdTEReNz4_TMeW_Q",
         "scope" : "GOOGLE",
         "types" : [ "school", "point_of_interest", "establishment" ],
         "user_ratings_total" : 54,
         "vicinity" : "23/8 Hoàng Việt"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 10.7996456,
               "lng" : 106.65358
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 10.80098682989272,
                  "lng" : 106.6550245298927
               },
               "southwest" : {
                  "lat" : 10.79828717010728,
                  "lng" : 106.6523248701073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/school-71.png",
         "id" : "a83a721ba718cee265353fb661398740854efc6e",
         "name" : "Trường Quốc tế Á Châu",
         "photos" : [
            {
               "height" : 398,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/102958877518470226801/photos\"\u003eHa Hoang\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAMAtKpP5vpf2fv39f1zOcZMcEmsbZ9oYgeoLE6avzFgMxsEG2bsjEwjHgGi1DPic0CVTQDXOnrb9rMGBiU4Kwl11PDKvw-LRS5v3YTHD2eLmjxtDrQ-_tj93OjV5UUqAdEhDj_zMhag6wQVHRT79XHY5mGhTBZ2S19IobmOYeSimCSlNf-2Fygg",
               "width" : 536
            }
         ],
         "place_id" : "ChIJ21IGADgpdTER14-tEUZD6l0",
         "plus_code" : {
            "compound_code" : "QMX3+VC Phường 12, Tân Bình, Hồ Chí Minh",
            "global_code" : "7P28QMX3+VC"
         },
         "rating" : 4,
         "reference" : "ChIJ21IGADgpdTER14-tEUZD6l0",
         "scope" : "GOOGLE",
         "types" : [ "school", "point_of_interest", "establishment" ],
         "user_ratings_total" : 5,
         "vicinity" : "Phường 12 Tân Bình, 156/20 Cộng Hòa"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 10.7516773,
               "lng" : 106.7177632
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 10.75302722989272,
                  "lng" : 106.7190701798927
               },
               "southwest" : {
                  "lat" : 10.75032757010728,
                  "lng" : 106.7163705201073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/school-71.png",
         "id" : "8f97e4d082e7ca302604bcf89b4d8c4fa3622a18",
         "name" : "Trường Tiểu Học Đặng Thùy Trâm",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 3672,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/111534643984742125028/photos\"\u003eNguyễn Trung Kiên\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAVQGr1GXlc0AvBnHSmcLBeJbeUeefPVs_fMnC1ko_duPEpZcuBYEhdoTaPrN_dOSzRILObzhcTvQMjs_a9QAuoHTpZQn-3uQU614cZ5yjc2TDlElGnW_49olvuJyojwGuEhAyfhyeHAA9DR9eW5H14KQoGhQiu4zm4_1PHEu46nQ0ZS8OGD3dCg",
               "width" : 4896
            }
         ],
         "place_id" : "ChIJ46JEjnsvdTERoP1QwXnu7UY",
         "plus_code" : {
            "compound_code" : "QP29+M4 Tân Thuận Tây, Quận 7, Hồ Chí Minh",
            "global_code" : "7P28QP29+M4"
         },
         "rating" : 4,
         "reference" : "ChIJ46JEjnsvdTERoP1QwXnu7UY",
         "scope" : "GOOGLE",
         "types" : [ "school", "point_of_interest", "establishment" ],
         "user_ratings_total" : 30,
         "vicinity" : "215 Trần Xuân Soạn"
      }
   ],
   "status" : "OK"
}