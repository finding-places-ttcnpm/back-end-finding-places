import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URI;

/**
 * Created by hoang on 5/5/2019.
 */
public class MiniClient extends WebSocketClient {

    public MiniClient(URI serverURI) {
        super(serverURI);
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        System.out.println("I connected to server");
        JSONObject js = new JSONObject()
                .put("code", "find_near_places")
                .put("lat", 10.8817689129033)
                .put("lng", 106.80890127146743)
                .put("category", "cafe")
                .put("radius", 1)
                .put("rating", 0)
                .put("isOpened", "false");
        this.send(js.toString());
    }

    @Override
    public void onMessage(String s) {
        System.out.println(s);

    }
    @Override
    public void onClose(int i, String s, boolean b) {

    }

    @Override
    public void onError(Exception e) {

    }

    public static void main(String[] args) throws Exception {
        MiniClient client = new MiniClient(new URI("ws://localhost:3025"));
        client.connect();
        while (client.getConnection() == null) {
        }
    }
}