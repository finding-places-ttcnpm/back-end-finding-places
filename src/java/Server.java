/**
 * Created by hoang on 5/5/2019.
 */

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.HashMap;

public class Server extends WebSocketServer {
    HashMap<String, RequestHandler> handler = null;

    public Server(int port) throws UnknownHostException {
        super(new InetSocketAddress(port));
        handler = new HashMap<String, RequestHandler>();

        InetAddress localhost = InetAddress.getLocalHost();
        System.out.println("Server's IP is " + localhost.getHostAddress().trim());
        System.out.println("server's port is " + this.getPort());
    }

    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        handler.put(webSocket.toString(), new RequestHandler(webSocket));

        System.out.println("Connection established from: " + webSocket.getRemoteSocketAddress().getHostString());
        System.out.println("New connection from " + webSocket.getRemoteSocketAddress().getAddress().getHostAddress());
    }

    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        handler.remove(webSocket.toString());
    }

    @Override
    public void onMessage(WebSocket webSocket, String s) {
        System.out.println(s);

        if (handler.containsKey(webSocket.toString())) {
            JSONObject js = new JSONObject(s);

            if (js == null) {
                System.out.println("Can not convert string to json object. May be wrong format string");
                return;
            }

            String code = js.getString("code");

            try {

                if (code.equals("login")) {
                    handler.get(webSocket.toString()).login(js.getString("user"), js.getString("pass"));
                } else if (code.equals("signup")) {
                    handler.get(webSocket.toString()).signUp(js);
                } else if (code.equals("get_place")) {
                    handler.get(webSocket.toString()).getPlace(js.getString("id"));
                } else if (code.equals("update_user_info")) {
                    handler.get(webSocket.toString()).updateUserInfo(js.getString("email"), js);
                } else if (code.equals("change_password")) {
                    handler.get(webSocket.toString()).changePassword(js.getString("email"), js);
                } else if (code.equals("mark_favorite_place")) {
                    handler.get(webSocket.toString()).markFavoritePlace(js.getString("email"), js.getString("idPlace"));
                } else if (code.equals("get_list_favorite_places")) {
                    handler.get(webSocket.toString()).getListFavoritePlaces(js.getString("email"));
                } else if (code.equals("insert_place")) {
                    handler.get(webSocket.toString()).insertPlace(js);
                } else if (code.equals("add_review")) {
                    handler.get(webSocket.toString()).addReview(js);
                } else if (code.equals("find_near_places")) {
                    handler.get(webSocket.toString()).findNearPlace(js);
                } else if (code.equals("find_name")) {
                    handler.get(webSocket.toString()).findName(js);
                } else if (code.equals("remove_favorite_place")) {
                    handler.get(webSocket.toString()).removeFavoritePlace(js);
                } else if (code.equals("get_user_info")) {
                    handler.get(webSocket.toString()).getUserInfo(js.getString("userID"));
                } else if (code.equals("most_added_recent")) {
                    handler.get(webSocket.toString()).getMostRecent();
                } else if (code.equals("most_review")) {
                    handler.get(webSocket.toString()).getMostReview();
                } else if (code.equals("forget_password")) {
                    handler.get(webSocket.toString()).forgetPassword(js);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Handler can not find RequestHandler for this Client!");
        }
    }

    @Override
    public void onError(org.java_websocket.WebSocket webSocket, Exception e) {

    }
}