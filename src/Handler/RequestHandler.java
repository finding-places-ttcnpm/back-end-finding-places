import com.google.gson.Gson;
import com.mongodb.client.FindIterable;
import com.sun.corba.se.spi.monitoring.MonitoringFactories;
import org.bson.Document;
import org.java_websocket.WebSocket;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.jws.soap.SOAPBinding;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RequestHandler {
    private WebSocket socket;

    public RequestHandler(WebSocket _socket) {
        this.socket = _socket;
    }

    void login(String user, String pass) throws IOException {
        User usr = (User) User.getModel(user, User.class);
        if (usr == null) {
            socket.send("-1"); //not exist
        } else {
            if (!pass.equals(usr.getPassword())) {
                socket.send("0"); //wrong email or pass
            } else {
                socket.send("1"); //success
            }
        }
    }

    void signUp(JSONObject js) throws IOException {
        System.out.println(js.toString());
        String email = js.getString("email");
        User usr = (User) DataModel.getModel(email, User.class);
        if (usr != null) {
            socket.send("-1"); //account has existed
        } else {
            User u = new User(js.getString("email"), js.getString("password"), js.getString("name"),
                    js.getString("dob"), js.getString("phone_number"));
            u.saveModel();
            socket.send("0"); //success
        }
    }

    void getPlace(String id) throws IOException {
        Place place = (Place) DataModel.getModel(id, Place.class);
        if (place == null) {
            socket.send("-1");
        } else {
            socket.send(place.getJson().toString());
        }
    }

    void updateUserInfo(String id, JSONObject js) {
        User usr = (User) DataModel.getModel(id, User.class);
        if (usr == null) {
            socket.send("-1");
            return;
        }

        usr.setName(js.getString("name"));
        usr.setDob(js.getString("dob"));
        usr.setPhoneNumber(js.getString("phoneNumber"));
        usr.updateModel();
        socket.send("0");
    }

    void changePassword(String id, JSONObject js) {
        User usr = (User) DataModel.getModel(id, User.class);
        if (usr == null) {
            socket.send("-2");
            return;
        }

        if (!js.getString("oldPassword").equals(usr.getPassword())) {
            socket.send("-1");
            return;
        }

        usr.setPassword(js.getString("newPassword"));
        usr.updateModel();
        socket.send("0");
    }

    void markFavoritePlace(String id, String idPlace) {
        User usr = (User) DataModel.getModel(id, User.class);
        if (usr == null) {
            socket.send("-1");
            return;
        }

        Place place = (Place) DataModel.getModel(idPlace, Place.class);
        if (place == null) {
            socket.send("-2");
            return;
        }

        usr.getFavoritePlacesID().add(idPlace);
        usr.updateModel();
        socket.send("0");
    }

    void getListFavoritePlaces(String id) {
        JSONObject js = new JSONObject();

        User usr = (User) DataModel.getModel(id, User.class);
        if (usr == null) {
            js.put("returnCode", "-1");
            socket.send(js.toString());
            return;
        }

        js.put("returnCode", "0");
        JSONArray jarr = new JSONArray(usr.getFavoritePlacesID());
        js.put("list", jarr);
        socket.send(js.toString());
    }

    void insertPlace(JSONObject req) {
        Detail detail = new Detail(req.getString("avatar"), req.getString("description"), req.getString("openTime"), req.getString("closeTime"));

        Place place = new Place(req.getString("name"), req.getDouble("latitude"), req.getDouble("longitude"),
                req.getString("address"), req.getString("category"), 0.0f, detail);
        place.saveModel();

        User usr = (User) DataModel.getModel(req.getString("userID"), User.class);
        if (usr == null) {
            socket.send("-1");
            return;
        }
        usr.getAddedPlaces().add(place.getId());
        usr.updateModel();

        //for most recent added place
        JSONObject mostRecent = MongoDB.getRecord("MostRecent", "Place");
        JSONArray jsArray = mostRecent.getJSONArray("list");
        List<String> lst = new ArrayList<String>();
        for (int i = 0; i < jsArray.length(); i++) {
            lst.add(jsArray.getString(i));
        }
        for (int i = lst.size() - 1; i > 0; i--) {
            lst.set(i, lst.get(i - 1));
        }
        lst.set(0, place.getId());
        jsArray = new JSONArray(lst);
        mostRecent.put("list", jsArray);
        MongoDB.updateRecord(mostRecent, "Place");

        socket.send("0");
    }

    void addReview(JSONObject req) {
        Review review = new Review(req.getInt("star"), req.getString("comment"), req.getString("userID"), req.getString("placeID"));
        review.saveModel();

        Place place = (Place) DataModel.getModel(req.getString("placeID"), Place.class);
        if (place == null) {
            socket.send("-1");
            return;
        }
        place.updateRating(review);
        place.updateModel();

        User usr = (User) DataModel.getModel(req.getString("userID"), User.class);
        usr.getReviewsID().add(review.getId());
        if (place == null) {
            socket.send("-2");
        }
        usr.updateModel();

        JSONObject mostReview = MongoDB.getRecord("MostReview", "Place");
        JSONArray jsArray = mostReview.getJSONArray("list");
        ArrayList<String> lst = new ArrayList<String>();
        int i;
        for (i = 0; i < jsArray.length(); i++) {
            if (jsArray.getString(i).length() == 0) {
                lst.add(place.getId());
                break;
            }

            Place p = (Place) DataModel.getModel(jsArray.getString(i), Place.class);
            if (p.getReviews().size() > place.getReviews().size()) {
                lst.add(p.getId());
            } else {
                lst.add(place.getId());
                break;
            }
        }
        for (int j = i; j < jsArray.length() && lst.size() <= 5; j++) {
            if (lst.contains(jsArray.getString(j))) continue;
            lst.add(jsArray.getString(j));
        }
        while (lst.size() < 5) {
            lst.add("");
        }

        jsArray = new JSONArray(lst);
        mostReview.put("list", jsArray);
        MongoDB.updateRecord(mostReview, "Place");

        socket.send("0");
    }

    void findNearPlace(JSONObject req) {
        double lat = req.getDouble("lat");
        double lng = req.getDouble("lng");
        double radius = 3.0;
        double rating = 0.0;
        boolean isOpened = false;
        String category = "";

        if (req.has("category")) {
            category = req.getString("category");
        }
        if (req.has("radius")) {
            radius = req.getDouble("radius");
        }
        if (req.has("rating")) {
            rating = req.getDouble("rating");
        }
        if (req.has("isOpened")) {
            if (req.get("isOpened").equals("true")) {
                isOpened = true;
            }
        }

        JSONArray arr = new JSONArray();
        FindIterable<Document> lst = MongoDB.getAllRecordCollection("Place");
        Gson gson = new Gson();

        for (Document document : lst) {
            JSONObject js = new JSONObject(document.toJson());
            if (js.getString("_id").equals("MostRecent") || js.getString("_id").equals("MostReview")) continue;

            Place place = (Place) gson.fromJson(js.toString(), Place.class);

            double distance = Utils.distanceBetweenLatLong(lat, lng, place.getLatitude(), place.getLongitude());
            boolean cond1 = true, cond2 = true, cond3 = true, cond4 = true;

            if (distance > radius) {
                cond1 = false;
            }
            if (place.getRating() < rating) {
                cond2 = false;
            }
            if (isOpened) {
                String start = place.getDetail().getOpenTime();
                String end = place.getDetail().getCloseTime();
                String curHour = LocalTime.now().getHour() + "";
                String curMinute = LocalTime.now().getMinute() + "";
                if (curMinute.length() < 2) curMinute = "0" + curMinute;
                String curTime = curHour + ":" + curMinute;
                if (!(curTime.compareTo(start) >= 0 && curTime.compareTo(end) <= 0)) {
                    cond3 = false;
                }
            }

            if (!category.equals("")) {
                if (!category.equals(place.getCategory())) {
                    cond4 = false;
                }
            }

            if (cond1 && cond2 && cond3 && cond4) {
                arr.put(place.getId());
            }
        }

        JSONObject res = new JSONObject();
        res.put("numPlaces", arr.length());
        res.put("list", arr);
        socket.send(res.toString());
    }

    void findName(JSONObject req) {
        FindIterable<Document> lst = MongoDB.searchByText(req.getString("text"), "name", "Place");
        JSONArray arr = new JSONArray();
        for (Document document : lst) {
            arr.put(document.getString("_id"));
        }

        JSONObject ans = new JSONObject();
        ans.put("list", arr);
        socket.send(ans.toString());
    }

    void removeFavoritePlace(JSONObject req) {
        User user = (User) DataModel.getModel(req.getString("userID"), User.class);

        boolean found = false;
        List<String> favoritePlacesID = user.getFavoritePlacesID();
        for (int i = 0; i < favoritePlacesID.size(); i++) {
            if (favoritePlacesID.get(i).equals(req.getString("placeID"))) {
                found = true;
                favoritePlacesID.remove(i);
                break;
            }
        }

        user.updateModel();
        if (found) {
            socket.send("0");
        } else {
            socket.send("-1");
        }
    }

    void getUserInfo(String userID) {
        User user = (User) DataModel.getModel(userID, User.class);
        if (user == null) {
            socket.send("-1");
        }

        JSONObject js = new JSONObject()
                .put("id", user.getId())
                .put("email", user.getEmail())
                .put("name", user.getName())
                .put("dob", user.getDob())
                .put("phoneNumber", user.getPhoneNumber())
                .put("addedPlaces", new JSONArray(user.getAddedPlaces()));
        socket.send(js.toString());
    }

    void getMostRecent() {
        JSONObject js = MongoDB.getRecord("MostRecent", "Place");
        JSONArray jsArray = js.getJSONArray("list");

        JSONObject res = new JSONObject();
        int number = 0;
        for (int i = 0; i < jsArray.length(); i++) {
            if (jsArray.getString(i).length() == 0) {
                continue;
            }
            String id = jsArray.getString(i);
            Place place = (Place) DataModel.getModel(id, Place.class);
            JSONObject detail = place.getJson();
            res.put(number + "", detail);
            number += 1;
        }

        res.put("number", number);
        socket.send(res.toString());
    }

    void getMostReview() {
        JSONObject js = MongoDB.getRecord("MostReview", "Place");
        JSONArray jsArray = js.getJSONArray("list");

        JSONObject res = new JSONObject();
        int number = 0;
        for (int i = 0; i < jsArray.length(); i++) {
            if (jsArray.getString(i).length() == 0) {
                continue;
            }
            String id = jsArray.getString(i);
            Place place = (Place) DataModel.getModel(id, Place.class);
            JSONObject detail = place.getJson();
            res.put(number + "", detail);
            number += 1;
        }

        res.put("number", number);
        socket.send(res.toString());
    }

    void forgetPassword(JSONObject info) {
        String id = info.getString("email");
        String name = info.getString("name");
        User user = (User) DataModel.getModel(id, User.class);

        if (user == null || !user.getName().toLowerCase().equals(name.toLowerCase())) {
            socket.send("-1");
        } else {
            user.setPassword("123456");
            user.updateModel();
            socket.send("0");
        }
    }
}