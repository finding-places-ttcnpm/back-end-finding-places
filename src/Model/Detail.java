/**
 * Created by hoang on 5/17/2019.
 */
public class Detail {
    private String avatar;
    private String description;
    private String openTime;
    private String closeTime;

    public Detail(String image, String description, String openTime, String closeTime) {
        this.avatar = image;
        this.description = description;
        this.openTime = openTime;
        this.closeTime = closeTime;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String image) {
        this.avatar = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }
}
