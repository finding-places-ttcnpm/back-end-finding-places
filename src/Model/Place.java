import javafx.scene.chart.XYChart;
import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hoang on 5/17/2019.
 */
public class Place extends DataModel {
    private String _id;
    private String name;
    private Double latitude;
    private Double longitude;
    private String address;
    private String category;
    private float rating;

    private Detail detail;
    List<String> reviews;

    public Place(String name, Double latitude, Double longitude, String address, String category, float rating, Detail detail) {
        this._id = new ObjectId().toHexString();
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
        this.category = category;
        this.rating = rating;
        this.detail = detail;

        this.reviews = new ArrayList<String>();
    }

    public String getId() {
        return _id;
    }

    public void setId(String id) {
        this._id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public List<String> getReviews() {
        return reviews;
    }

    public void setReviews(List<String> reviews) {
        this.reviews = reviews;
    }

    public Detail getDetail() {
        return detail;
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
    }

    public JSONObject getJson() {
        JSONObject js = new JSONObject()
                .put("id", this.getId())
                .put("name", this.getName())
                .put("latitude", this.getLatitude())
                .put("longitude", this.getLongitude())
                .put("address", this.getAddress())
                .put("category", this.getCategory())
                .put("rating", this.getRating())

                .put("avatar", this.detail.getAvatar())
                .put("description", this.detail.getDescription())
                .put("openTime", this.detail.getOpenTime())
                .put("closeTime", this.detail.getCloseTime());

        JSONArray res = new JSONArray();
        for (String idReview : this.getReviews()) {
            Review review = (Review) DataModel.getModel(idReview, Review.class);
            User usr = (User) DataModel.getModel(review.getIdUser(), User.class);

            JSONArray arr = new JSONArray();
            arr.put(review.getStar());
            arr.put(review.getComment());
            arr.put(review.getDatetime());
            arr.put(usr.getId());
            arr.put(usr.getName());

            res.put(arr);
        }

        js.put("reviews", res);
        return js;
    }

    public void updateRating(Review review) {
        int oldSum = (int) this.getRating() * this.getReviews().size();
        int newSum = oldSum + review.getStar();
        this.setRating(((float) newSum) / (this.getReviews().size() + 1));
        this.getReviews().add(review.getId());
    }
}
