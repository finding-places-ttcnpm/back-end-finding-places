import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by hoang on 5/17/2019.
 */
public class User extends DataModel {
    private String _id;
    private String email;
    private String password;
    private String name;
    private String dob;
    private String phoneNumber;

    private List<String> addedPlaces;
    private List<String> reviewsID;
    private List<String> favoritePlacesID;

    public User(String email, String password, String name, String dob, String phoneNumber) {
        super();
        this._id = email;
        this.email = email;
        this.password = password;
        this.name = name;
        this.dob = dob;
        this.phoneNumber = phoneNumber;

        this.addedPlaces = new ArrayList<String>();
        this.reviewsID = new ArrayList<String>();
        this.favoritePlacesID = new ArrayList<String>();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<String> getAddedPlaces() {
        return addedPlaces;
    }

    public void setAddedPlaces(List<String> addedPlaces) {
        this.addedPlaces = addedPlaces;
    }

    public List<String> getFavoritePlacesID() {
        return favoritePlacesID;
    }

    public void setFavoritePlacesID(List<String> favoritePlacesID) {
        this.favoritePlacesID = favoritePlacesID;
    }

    public List<String> getReviewsID() {
        return reviewsID;
    }

    public void setReviewsID(List<String> reviewsID) {
        this.reviewsID = reviewsID;
    }

    public String getId() {
        return _id;
    }

    public void setId(String id) {
        this._id = id;
    }
}