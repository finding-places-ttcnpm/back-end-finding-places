import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.Filters;
import org.bson.Document;

/**
 * Created by hoang on 5/18/2019.
 */
public class TestCode {
    public static void main(String[] args) {
        MongoClientURI uri = new MongoClientURI(
                "mongodb://ruakon:Ruakon123456@findingplaces-shard-00-00-q4arq.mongodb.net:27017,findingplaces-shard-00-01-q4arq.mongodb.net:27017,findingplaces-shard-00-02-q4arq.mongodb.net:27017/test?ssl=true&replicaSet=FindingPlaces-shard-0&authSource=admin&retryWrites=true");

        MongoClient mongoClient = new MongoClient(uri);
        MongoDatabase database = mongoClient.getDatabase("FindingPlaces");
        MongoCollection coll = database.getCollection("Place");

        coll.createIndex(Indexes.text("name"));

        FindIterable<Document> lst= coll.find(Filters.text("Tân Sơn Nhất"));
        for (Document document : lst) {
            System.out.println(document.getString("name"));
        }
    }
}