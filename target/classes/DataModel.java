import com.google.gson.Gson;
import org.json.JSONObject;

/**
 * Created by hoang on 5/17/2019.
 */
public abstract class DataModel {
    private static Gson gson = new Gson();

    public DataModel() {
    }

    public void saveModel() {
        JSONObject js = new JSONObject(gson.toJson(this));
        MongoDB.insertRecord(js, this.getClass().getSimpleName());
    }

    public static Object getModel(String id, Class c) {
        JSONObject js = MongoDB.getRecord(id, c.getSimpleName());
        if (js == null) {
            System.out.println("Object with id " + id + " does not exist in database");
            return null;
        }
        return gson.fromJson(js.toString(), c);
    }

    public void updateModel() {
        JSONObject js = new JSONObject(gson.toJson(this));
        MongoDB.updateRecord(js, this.getClass().getSimpleName());
    }
}
