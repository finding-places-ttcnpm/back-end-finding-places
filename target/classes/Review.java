import org.bson.types.ObjectId;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hoang on 5/17/2019.
 */
public class Review extends DataModel {
    private String _id;
    private int star;
    private String datetime;
    private String comment;
    private String idUser;
    private String idPlace;

    public Review (int star, String comment, String _idUser, String _idPlace) {
        this._id = new ObjectId().toHexString();
        this.star = star;
        SimpleDateFormat ft = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        this.datetime = ft.format(new Date());
        this.comment = comment;
        this.idUser = _idUser;
        this.idPlace = _idPlace;
    }

    public String getId() {
        return _id;
    }

    public void setId(String id) {
        this._id = id;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public String getDatetime() {
        return this.datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdPlace() {
        return idPlace;
    }

    public void setIdPlace(String idPlace) {
        this.idPlace = idPlace;
    }
}