import java.io.InputStream;
import java.util.Properties;

public class ConfigReader {
    private static Properties prop = null;
    private static InputStream input = null;

    public ConfigReader() {
        loadConfig();
    }

    private static void loadConfig() {
        prop = new Properties();
        input = ConfigReader.class.getClassLoader().getResourceAsStream("config.properties");
        if (input == null) {
            System.out.println("Unable to find config.properties");
        }

        try {
            prop.load(input);
        } catch (Exception e) {
            System.out.println("Error loading config file: " + e.toString());
            e.printStackTrace();
        }
    }

    public synchronized static String getProperty(String key) {
        if (prop == null) {
            loadConfig();
        }

        return prop.getProperty(key);
    }
}
