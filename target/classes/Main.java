

public class Main {
    public static void main(String[] args) {
        MongoDB db = new MongoDB();
        db.connectDatabase();

        int port = Integer.parseInt(ConfigReader.getProperty("server.port"));

        Server server = null;
        try {
            server = new Server(port);
        } catch (Exception e) {
            System.out.println("The server could not be created");
            e.printStackTrace();
            return;
        }

        server.start();
    }
}