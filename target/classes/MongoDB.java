import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Indexes;
import com.mongodb.util.JSON;
import org.bson.*;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.JSONObject;
import org.omg.CORBA.*;

import java.util.HashMap;

import static com.mongodb.client.model.Filters.eq;

public class MongoDB {
    private static MongoClient mongoClient = null;
    private static MongoDatabase database = null;
    private static HashMap<String, MongoCollection> collections = null;

    public void connectDatabase() {
        MongoClientURI uri = new MongoClientURI(
                "mongodb://" + ConfigReader.getProperty("db.user") + ":" + ConfigReader.getProperty("db.password") + "@findingplaces-shard-00-00-q4arq.mongodb.net:27017,findingplaces-shard-00-01-q4arq.mongodb.net:27017,findingplaces-shard-00-02-q4arq.mongodb.net:27017/test?ssl=true&replicaSet=FindingPlaces-shard-0&authSource=admin&retryWrites=true");

        mongoClient = new MongoClient(uri);
        database = mongoClient.getDatabase(ConfigReader.getProperty("db.database"));

        collections = new HashMap<String, MongoCollection>();
    }

    private static MongoCollection getCollection(String name) {
        if (!collections.containsKey(name)) {
            MongoCollection c = database.getCollection(name);
            collections.put(name, c);
        }

        return collections.get(name);
    }

    public static void insertRecord(JSONObject record, String collectionName) {
        MongoCollection coll = getCollection(collectionName);
        Document document = Document.parse(record.toString());

        coll.insertOne(document);
    }

    public static JSONObject getRecord(String id, String collectionName) {
        MongoCollection coll = getCollection(collectionName);
        Document d = (Document) coll.find(eq("_id", id)).limit(1).first();

        if (d == null) return null;
        return new JSONObject(d.toJson());
    }

    public static void updateRecord(JSONObject record, String collectionName) {
        MongoCollection coll = getCollection(collectionName);

        coll.replaceOne(eq("_id", record.getString("_id")), Document.parse(record.toString()));
    }

    public static FindIterable<Document> getAllRecordCollection(String collectionName) {
        MongoCollection coll = getCollection(collectionName);
        return coll.find();
    }

    public static FindIterable<Document> searchByText(String searchText, String field, String collectionName) {
        MongoCollection coll = getCollection(collectionName);
        coll.createIndex(Indexes.text(field));
        return coll.find(Filters.text(searchText));
    }
}