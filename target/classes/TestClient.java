import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class TestClient {
    public static void main(String[] args) throws Exception{
        Socket socket = new Socket("localhost", 3025);

        JSONObject obj = new JSONObject()
                .put("name", "Trương Hoàng Huy")
                .put("age", 21);

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        bw.write(obj.toString() + "\n");
        bw.flush();
        System.out.println(obj.toString());
        while (true);
    }
}
