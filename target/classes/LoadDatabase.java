import com.mongodb.util.JSON;
import org.codehaus.jettison.json.JSONArray;
import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONObject;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URI;

public class LoadDatabase extends WebSocketClient {
    public LoadDatabase(URI serverURI) {
        super(serverURI);
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        System.out.println("I connected to server");

        /*JSONObject js = new JSONObject()
                .put("code", "signup")
                .put("email", "truonghoanghuy@gmail.com")
                .put("password", "ruakon")
                .put("name", "Trương Hoàng Huy")
                .put("dob", "15/04/1998")
                .put("phone_number", "0963985789");
        this.send(js.toString());*/

        /*try {
            File folder = new File("D:\\Project\\Java\\back-end-finding-places\\places");

            for (File f : folder.listFiles()) {
                FileInputStream fis = new FileInputStream(f);
                byte[] data = new byte[(int) f.length()];
                fis.read(data);
                fis.close();
                String str = new String(data, "UTF-8");

                String imageString = null;
                String type = "png";
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                BufferedImage image = null;
                try {
                    image = ImageIO.read(LoadDatabase.class.getResource("image.png"));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

                try {
                    ImageIO.write(image, type, bos);
                    byte[] imageBytes = bos.toByteArray();

                    BASE64Encoder encoder = new BASE64Encoder();
                    imageString = encoder.encode(imageBytes);

                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                JSONObject js = null;
                try {
                    js =  new JSONObject(str);
                } catch (Exception e) {
                    System.out.println(f.getName());
                    continue;
                }
                org.json.JSONArray arr = js.getJSONArray("results");
                for (int i = 0; i < arr.length(); ++i) {
                    JSONObject cur = arr.getJSONObject(i);
                    String name = cur.getString("name");
                    double lat = cur.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
                    double lng = cur.getJSONObject("geometry").getJSONObject("location").getDouble("lng");
                    String address = cur.getString("vicinity");
                    String category = f.getName().substring(0, f.getName().length() - 4);
                    double star = cur.getInt("rating");

                    //Detail
                    String img = imageString;
                    String des = "Not available";
                    String openTime = "7:30";
                    String closedTime = "21:00";

                    //review
                    int rate = (int) star;
                    String datetime = "19:25:36 29/04/2019";
                    String comment = "";

                    JSONObject places = new JSONObject()
                            .put("name", name)
                            .put("latitude", lat)
                            .put("longitude", lng)
                            .put("address", address)
                            .put("category", category)

                            .put("avatar", img)
                            .put("description", des)
                            .put("openTime", openTime)
                            .put("closeTime", closedTime);

                    JSONObject review = new JSONObject()
                            .put("star", rate)
                            .put("datetime", datetime)
                            .put("comment", comment);

                    places.put("code", "insert_place");
                    places.put("userID", "truonghoanghuy@gmail.com");
                    this.send(places.toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        /*JSONObject review = new JSONObject()
                .put("code", "add_review")
                .put("userID", "truonghoanghuy@gmail.com")
                .put("placeID", "5cea845a5502926e8fd574a7")
                .put("star", 3)
                .put("comment", "Quá chật chội, kẹt xe!!!");
        this.send(review.toString());*/
    }

    @Override
    public void onMessage(String s) {
        System.out.println(s);
    }
    @Override
    public void onClose(int i, String s, boolean b) {

    }

    @Override
    public void onError(Exception e) {

    }

    public static void main(String[] args) throws Exception {
        LoadDatabase client = new LoadDatabase(new URI("ws://localhost:3025"));
        client.connect();
    }
}